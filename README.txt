    ____                 ______                       _       _
   / __ \___  ____ _____/ / __ \___  ____ ___  ____ _(_)___  (_)___  ____ _
  / /_/ / _ \/ __ `/ __  / /_/ / _ \/ __ `__ \/ __ `/ / __ \/ / __ \/ __ `/
 / _, _/  __/ /_/ / /_/ / _, _/  __/ / / / / / /_/ / / / / / / / / / /_/ /
/_/ |_|\___/\__,_/\__,_/_/ |_|\___/_/ /_/ /_/\__,_/_/_/ /_/_/_/ /_/\__, /
                                                                  /____/

README

Tell your readers how long they'll need to get through all of your gibberish,
with this clever module.
With ReadRemaining.js TL;DR is a thing of the past.

The ReadRemaining module implements the ReadRemaining JS library
(https://aerolab.github.io/readremaining.js)

-- GETTING STARTED --

1. Install ReadRemaining in the usual way
   (https://www.drupal.org/node/1897420)
2. Download the ReadRemaining library (version 1.0.0)
   (https://github.com/Aerolab/readremaining.js),
   rename the folder to readremaining and place it in /libraries/
   in your drupal root.
3. Go to Administration > Configuration > System > ReadRemaining
   (/admin/config/system/readremaining)
4. Select the contenttypes ReadRemaining should be active on
5. Optionally change the DOM selector
6. Optionally change the look and feel
7. Optionally change the javascript settings
8. Click "Save configuration"

Now visit a node of a selected contenttype and start reading,
ReadRemaining will now appear in a personalized, non-annoying way.

-- MAINTAINERS --

Fons Vandamme - https://www.drupal.org/u/fons-vandamme
